/* eslint-disable @typescript-eslint/no-var-requires */
/*
 * Modify the webpack config by exporting an Object or Function.
 *
 * If the value is an Object, it will be merged into the final
 * config using `webpack-merge`.
 *
 * If the value is a function, it will receive the resolved config
 * as the argument. The function can either mutate the config and
 * return nothing, OR return a cloned or merged version of the config.
 *
 * https://cli.vuejs.org/config/#configurewebpack
 */

const path = require('path');

// eslint-disable-next-line @typescript-eslint/no-unused-vars
module.exports = (config) => ({
  entry: {
    mainCss: [path.join(__dirname, './src/assets/styles/_main.scss')],
    tailwindCss: [
      path.join(__dirname, './src/assets/styles/thirdparty/_tailwindcss.scss'),
    ],
  },
  module: {
    rules: [
      {
        test: /\.wasm$/,
        loaders: ['wasm-loader'],
      },
    ],
  },
  resolve: {
    alias: {
      '~': path.join(__dirname, './'),
      '@': path.join(__dirname, './src/app'),
      '@assets': path.join(__dirname, './src/assets'),
      '@fonts': path.join(__dirname, './src/assets/fonts'),
      '@images': path.join(__dirname, './src/assets/images'),
      '@styles': path.join(__dirname, './src/assets/styles'),
      '@components': path.join(__dirname, './src/app/components'),
      '@router': path.join(__dirname, './src/app/router'),
      '@views': path.join(__dirname, './src/app/views'),
      '@providers': path.join(__dirname, './src/app/providers'),
      '@layouts': path.join(__dirname, './src/app/layouts'),
      '@plugins': path.join(__dirname, './src/app/plugins'),
      '@binary-tree/binary-tree-module': path.join(
        __dirname,
        '../../libs/binary-tree-module/src/index.ts'
      ),
    },
    modules: ['node_modules'],
    extensions: [
      '.js',
      '.ts',
      '.tsx',
      '.wasm',
      '.vue',
      '.scss',
      '.css',
      '.sass',
    ],
  },
});
