module.exports = {
  displayName: 'binary-tree',
  preset: '../../jest.preset.js',
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+\\.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'vue', 'js', 'json', 'wasm'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/app/$1',
    '^~/(.*)$': '<rootDir>/src/$1',
    '^@assets/(.*)$': '<rootDir>/src/assets/$1',
    '^@fonts/(.*)$': '<rootDir>/src/assets/fonts/$1',
    '^@images/(.*)$': '<rootDir>/src/assets/images/$1',
    '^@styles/(.*)$': '<rootDir>/src/assets/styles/$1',
    '^@components/(.*)$': '<rootDir>/src/app/components/$1',
    '^@router/(.*)$': '<rootDir>/src/app/router/$1',
    '^@views/(.*)$': '<rootDir>/src/app/views/$1',
    '^@providers/(.*)$': '<rootDir>/src/app/providers/$1',
    '^@layouts/(.*)$': '<rootDir>/src/app/layouts/$1',
    '^@plugins/(.*)$': '<rootDir>/src/app/plugins/$1',
  },
  collectCoverage: true,

  coverageDirectory: '../../coverage/apps/binary-tree',
  snapshotSerializers: ['jest-serializer-vue'],
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/tsconfig.spec.json',
      babelConfig: '<rootDir>/babel.config.js',
    },
    'vue-jest': {
      tsConfig: `${__dirname}/tsconfig.spec.json`,
      babelConfig: `${__dirname}/babel.config.js`,
    },
  },
};
