import { Component, Vue } from 'vue-property-decorator';

@Component<App>({
  name: 'app',
  components: {
    Header: () => import('@layouts/header/index.vue'),
    Footer: () => import('@layouts/footer/index.vue'),
  },
  render(h) {
    return h('div', 'hello :)');
  },
})
export default class App extends Vue {
  public mounted(): void {}
}
