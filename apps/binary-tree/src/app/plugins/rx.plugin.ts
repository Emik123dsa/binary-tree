import Vue from 'vue';
import VueRx from 'vue-rx';

import {
  Subject,
  BehaviorSubject,
  ReplaySubject,
  AsyncSubject,
  Observable,
} from 'rxjs';

Vue.use(VueRx, {
  Subject,
  BehaviorSubject,
  ReplaySubject,
  AsyncSubject,
  Observable,
});
