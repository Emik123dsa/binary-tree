import { RouteConfig } from 'vue-router';

export const routes: RouteConfig[] = [
  {
    name: 'index',
    path: '/',
    component: () => import('@views/index/index.vue'),
  },
];
