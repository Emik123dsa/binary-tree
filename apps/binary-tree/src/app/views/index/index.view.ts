import { Vue, Component } from 'vue-property-decorator';

/**
 * Define index view;
 *
 * @export
 * @class IndexView
 * @decorator {Component}
 * @extends {Vue}
 */
@Component<IndexView>({
  name: 'indexView',
  components: {
    BinaryTreeExample: () =>
      import('@components/binary-tree-example/index.vue'),
  },
})
export default class IndexView extends Vue {
  public mounted(): void {
    this.$zhopa();
  }
}
