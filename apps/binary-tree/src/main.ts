import Vue from 'vue';
import VueRouter from 'vue-router';
import '@plugins';
import { createRouter } from '@router';

import App from '@/index.vue';
Vue.config.productionTip = false;

export const createApp: () => void = (): void => {
  const ɵApp: string = '#app';
  const router: VueRouter = createRouter();
  void new Vue({
    router,
    render: (h: Vue.CreateElement): Vue.VNode => h(App),
  }).$mount(ɵApp);
};

createApp();
