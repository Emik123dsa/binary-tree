module.exports = {
  purge: [
    './src/**/*.vue',
    './src/**/*.html',
    './src/**/*.ts',
    './src/**/*.tsx',
  ],
  darkMode: false,
  theme: {
    extend: {
      padding: {},
    },
    container: {
      center: true,
      padding: '1.5rem',
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
