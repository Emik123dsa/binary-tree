/**
 * ɵ environment;
 *
 * @interface ɵenvironment
 */
declare interface ɵenvironment {
  ɵapp: string;
  ɵproduction: boolean;
  ɵhmr: boolean;
}
