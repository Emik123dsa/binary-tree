#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
// #include <emscripten/emscripten.h>

#ifdef _cplusplus
extern "C" {
#endif

// /**
//  * Player protobuf impl;
//  */
// message Player {
//   required uint32 id = 1;
//   required string uuid = 2;
//   optional string name = 3;
// }

// /**
//  * Node protobuf impl;
//  */
// message Node {
//   required string name = 1;
//   required uint32 value = 2;

//   repeated Node children = 4;
//   repeated Player players = 3;
// }

// /**
//  * Binary Tree protobuf impl;
//  */
// message BinaryTree {
//   repeated Node node = 1;
// }


typedef struct {
  int id;
  char* uuid;
  char* name;
} bt_player_t;

typedef struct {
  char *name;
  int value;
  struct bt_player_t** players;
} bt_node_t;

typedef struct {

} bt_t;

int main(int argc, char ** argv) {
  bt_player_t * bpt;
  bpt = (bt_player_t * )malloc(sizeof(bt_player_t));
  if (bpt == NULL) exit(0);
  bpt->id = 1;
  bpt->name = "hello :)";
  free(bpt);
  // const char* bt_players[] = {"vova", "dm", "hello :)"};
  // size_t bt_player_size = sizeof(bt_players) / sizeof(char*);
  // for(size_t _i =0;_i<bt_player_size;++_i) {
  //   printf("%s\f", bt_players[_i]);
  // }

  return 0;
}

#ifdef _cplusplus
}
#endif
