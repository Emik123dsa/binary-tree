import Vue, { VueConstructor } from 'vue';

declare module 'vue/types/vue' {
  interface Vue {
 
  }
}

/**
 * Vue binary tree instance;
 *
 * @export
 * @param {VueConstructor<Vue>} Vue
 */
export function VueBinaryTree(Vue: VueConstructor<Vue>): void {
  Vue.mixin({
    beforeCreate(): void {},
  });

}
