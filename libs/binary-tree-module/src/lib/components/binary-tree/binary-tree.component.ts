import 'reflect-metadata';
import * as d3 from 'd3';
import { Component, Mixins } from 'vue-property-decorator';
import { BinaryTreePropsMixin } from './../../mixins/binary-tree/binary-tree.props.mixin';

const BINARY_TREE_NAME: string = 'binaryTree';

@Component<BinaryTree>({
  name: BINARY_TREE_NAME,
})
export default class BinaryTree extends Mixins(BinaryTreePropsMixin) {

  /**
   * Define binary tree instance;
   *
   * @readonly
   * @private
   * @type {d3.HierarchyPointNode<unknown>}
   * @memberof BinaryTree
   */
  private get _btMapInstance(): d3.HierarchyPointNode<unknown> {
    return d3
      .tree()
      .separation(
        (a: d3.HierarchyPointNode<{}>, b: d3.HierarchyPointNode<{}>) =>
          a.parent === b.parent ? 0x1 : 0x1
      )
      .size([this.BT_CLIENT_WIDTH, this.BT_CLIENT_HEIGHT])(
      this._btRoot || null
    );
  }

  /**
   * Init binary tree;
   *
   * @memberof BinaryTree
   */
  public mounted(): void {
    this.btMapInit();
  }

  private get _btMapViewBox(): Readonly<number[]> {
    return [
      240 - this.BT_CLIENT_WIDTH,
      -(this.BT_CLIENT_HEIGHT / 2 + 110 * 3),
      this.BT_CLIENT_WIDTH,
      this.BT_CLIENT_HEIGHT,
    ];
  }

  /**
   * Binary tree map init;
   *
   * @memberof BinaryTree
   */
  public btMapInit(): void {
    this._btRef = d3
      .select('svg.bt')
      .attr('viewBox', this._btMapViewBox as any);
    this._btContainerRef = this._btRef.select('g.bt-container');
    void this.btMapInitData().then(($root: any): void => {
      void this.btMapInitZoom();
      void this.btMapUpdate($root);
    });
  }

  /**
   * Binary tree init data;
   *
   * @param {number} [hight]
   * @memberof BinaryTree
   */
  public btMapInitData(hight?: number): Promise<void> {
    return new Promise(
      (
        res: ($root: any) => void | PromiseLike<void>,
        rej: (reason?: unknown) => void
      ) => {
        try {
          if (!this.getTreeData && !Array.isArray(this.getTreeData))
            throw new ReferenceError(this.getTreeData.toString());
          this._btRoot = d3.hierarchy<unknown>(
            this.getTreeData ?? [],
            (d: any) => d.children
          );
          this._btRoot.x0 = this.BT_CLIENT_HEIGHT / 2;
          this._btRoot.y0 = 0;
          return res(this._btRoot);
        } catch (e) {
          return rej(e?.message);
        }
      }
    );
  }

  /**
   * Binray tree init map zoom;
   *
   * @param {number} [clientHeight]
   * @memberof BinaryTree
   */
  public btMapInitZoom(clientHeight?: number): void {
    const zoomFactory: d3.ZoomBehavior<Element, unknown> = d3
      .zoom()
      .scaleExtent(this.BT_SCALE_ZOOM)
      .on('zoom', (event) =>
        d3.select('g.bt-container').attr('transform', event.transform)
      );
    void d3.zoomIdentity.translate(0, clientHeight || 0).scale(1);
    void this._btRef.call(zoomFactory).on('dblclick', null);
  }

  /**
   * Commit ellbows of binary tree;
   *
   * @readonly
   * @private
   * @memberof BinaryTree
   */
  private get _btMapElbow(): (s: any, d: any) => string {
    return (s: any, d: any): string => {
      return `M${-s.y}, ${-s.x}
              H${-d.y + (d.y - s.y) / 2}
              V${-d.x}
              H${-d.y}`;
    };
  }

  private get _btMapTransitionElbow(): (s: any, d: any) => string {
    return (s: any, d: any): string => {
      return `M${-s.y},${-s.x}
              H${-d.y}
              V${-s.x}
              H${-s.y}`;
    };
  }

  /**
   * Binary tree map on update;
   *
   * @param {*} $root
   * @memberof BinaryTree
   */
  public btMapUpdate($root: any): void {
    /** @var d3.HierarchyNode<unknown> */
    this._btNodes = this._btMapInstance.descendants();

    /** @var Array<d3.HierarchyPointNode<unknown>>; */
    this._btLinks = this._btMapInstance.descendants().slice(0x1);

    /** @var void Rebuild binary tree children; */
    void this._btMapRebuildChildren();

    /** @var void Build binary tree nodes; */
    void this._btMapBuildNodes($root);

    /** @var void Build binray tree links; */
    void this._btMapBuildLinks($root);

    /** @var void Reset binary tree; */
    void this._btMapStashChildren();
  }

  /**
   * Rebuild binary tree childrens;
   *
   * @private
   * @memberof BinaryTree
   */
  private _btMapRebuildChildren(): void {
    void this._btNodes?.forEach(
      (d: d3.HierarchyNode<{}> & { y: number }) =>
        (d.y = d.depth * this.BT_RATIO_DEEPTH)
    );
  }

  /**
   * Build binary tree nodes;
   *
   * @private
   * @param {*} $root
   * @memberof BinaryTree
   */
  private _btMapBuildNodes($root: any): void {
    /**
     * Create instance of binary tree node;
     */
    const node = this._btContainerRef
      .selectAll('g.bt-node')
      .data(this._btNodes, (d: any) => d?.id || (d.id = ++this._btIndex));

    /**
     * Enter binary tree nodes;
     */
    const nodeEnter = node
      .enter()
      .append('g')
      .attr('class', 'bt-node')
      .on('click', () => {
        console.log('clicked');
      })
      .attr(
        'transform',
        () => `translate(${$root?.x0 || 0},${$root?.y0 || 0})`
      );
    nodeEnter
      .append('rect')
      .attr('class', 'bt-node')
      .attr('id', (d: any) => d.id)
      .attr('width', 1e-10)
      .attr('height', 1e-10)
      .style('fill', 'none')
      .style('stroke', 'none')
      .style('stroke-width', 1e-10)
      .style('cursor', 'pointer');

    /**
     * Update binary tree nodes;
     */
    const nodeUpdate = nodeEnter
      .merge(node)
      .transition()
      .duration(this.BT_DURATION)
      .attr('transform', ({ y, x }) => `translate(${-y},${-x})`);
    nodeUpdate
      .select('rect.bt-node')
      .style('fill', '#fff')
      .attr('width', 220)
      .attr('height', 40)
      .attr('x', -110)
      .attr('y', -20)
      .style('fill', '#fff')
      .style('stroke', '#8ca68c')
      .style('stroke-width', 4);
    nodeUpdate.select('text').style('fill-opacity', 1);

    /**
     * Exit binary tree nodes;
     */
    const nodeExit = node
      .exit()
      .transition()
      .duration(this.BT_DURATION)
      .attr('transform', () => `translate(${$root?.x0 || 0},${$root?.y0 || 0})`)
      .remove();
    nodeExit
      .select('rect.bt-node')
      .attr('width', 1e-10)
      .attr('height', 1e-10)
      .style('fill', 'none')
      .style('stroke', 'none')
      .style('stroke-width', 1e-10);
    nodeExit.select('text').style('fill-opacity', 0);
  }

  /**
   * Build binary tree links;
   *
   * @private
   * @param {*} $root
   * @memberof BinaryTree
   */
  private _btMapBuildLinks($root: any): void {
    /**
     * Enter tree links;
     */
    const link = this._btContainerRef
      .selectAll('path.bt-link')
      .data(this._btLinks, ({ id }) => id);
    const linkEnter = link
      .enter()
      .insert('path', 'g')
      .attr('class', 'bt-link')
      .attr('d', () => {
        const o = {
          x: $root?.x0 || 0,
          y: $root?.y0 || 0 + this.BT_CLIENT_WIDTH / 2,
        };
        return this._btMapTransitionElbow(o, o);
      })
      .attr('fill', 'none')
      .attr('stroke-width', 2)
      .attr('stroke', 'purple');

    /**
     * Update tree links;
     */
    const linkUpdate = linkEnter.merge(link);
    linkUpdate
      .transition()
      .duration(this.BT_DURATION)
      .attr('d', (d: any) => this._btMapElbow(d, d?.parent));

    /**
     * Exit tree links;
     */
    link
      .exit()
      .transition()
      .duration(this.BT_DURATION)
      .attr('d', () => {
        const o = {
          x: $root?.x || 0,
          y: $root?.y || 0 + this.BT_CLIENT_WIDTH / 2,
        };
        return this._btMapTransitionElbow(o, o);
      })
      .remove();
  }

  /**
   * Stash binary tree children;
   *
   * @private
   * @memberof BinaryTree
   */
  private _btMapStashChildren<T = d3.HierarchyPointNode<unknown>>(): void {
    this._btNodes.forEach((node: any) => {
      node.x0 = node.x;
      node.y0 = node.y;
    });
  }
}
