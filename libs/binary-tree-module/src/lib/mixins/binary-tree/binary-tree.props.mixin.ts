import * as d3 from 'd3';

import { Vue, Component } from 'vue-property-decorator';

const BT_MIXIN_PROPS_NAME: string = 'binaryTreePropsMixin';

@Component<BinaryTreePropsMixin>({
  name: BT_MIXIN_PROPS_NAME,
})
export class BinaryTreePropsMixin extends Vue {
  /**
   * Define binary tree reference;
   *
   * @type {*}
   * @memberof BinaryTree
   */
  public get btRef(): any {
    return this._btRef;
  }
  public set btRef($btRef: any) {
    this._btRef = $btRef;
  }
  protected _btRef: any;

  /**
   * Define current binary tree index;
   *
   * @type {number}
   * @memberof BinaryTree
   */
  public get btIndex(): number {
    return this._btIndex;
  }
  public set btIndex($btIndex: number) {
    this._btIndex = $btIndex;
  }
  protected _btIndex: number = 0;

  /**
   * Define binary tree root;
   *
   * @type {number}
   * @memberof BinaryTree
   */
  public get btRoot(): any {
    return this._btRoot;
  }
  public set btRoot($btRoot: any) {
    this._btRoot = $btRoot;
  }
  protected _btRoot: any;

  /**
   * Define binray tree container ref;
   *
   * @type {*}
   * @memberof BinaryTree
   */
  public get btContainerRef(): any {
    return this._btContainerRef;
  }
  public set btContainerRef($btContainerRef: any) {
    this._btRoot = $btContainerRef;
  }
  protected _btContainerRef: any;

  /**
   * Binary tree nodes;
   *
   * @protected
   * @type {any[]}
   * @memberof BinaryTree
   */
  public get btNodes(): any {
    return this._btNodes;
  }
  public set btNodes($btNodes: any) {
    this._btNodes = $btNodes;
  }
  protected _btNodes: d3.HierarchyPointNode<{}>[] = [];

  /**
   * Define binary tree links;
   *
   * @type {*}
   * @memberof BinaryTree
   */
  public get btLinks(): any {
    return this._btLinks;
  }
  public set btLinks($btLinks: any) {
    this._btLinks = $btLinks;
  }
  protected _btLinks: any[] = [];

  /**
   * Select binary tree node;
   * we need to define this variety in the
   * case of drag/drop evolution;
   *
   * @protected
   * @type {*}
   * @memberof BinaryTree
   */
  public get btSelectedNode(): any {
    return this._btSelectedNode;
  }
  public set btSelectedNode($btSelectedNode: any) {
    this._btSelectedNode = $btSelectedNode;
  }
  protected _btSelectedNode: any = null;

  /**
   * Default animated duration;
   *
   * @protected
   * @type {number}
   * @memberof BinaryTree
   */
  protected readonly BT_DURATION: number = 0x3e8;

  /**
   * Define default scale zoom;
   *
   * @protected
   * @type {[number, number]}
   * @memberof BinaryTree
   */
  protected readonly BT_SCALE_ZOOM: [number, number] = [0x1 / 0x2, 0x8];

  /**
   * Define binary tree deepth;
   *
   * @protected
   * @type {number}
   * @memberof BinaryTree
   */
  protected readonly BT_RATIO_DEEPTH: number = (0xb4 * 0x3) / 0x2;

  protected get BT_CLIENT_WIDTH(): Readonly<number> {
    return document.body.clientWidth;
  }

  protected get BT_CLIENT_HEIGHT(): Readonly<number> {
    return document.body.clientHeight;
  }

  protected get getTreeData() {
    return {
      name: 'flare',
      children: [
        {
          name: 'animate',
          children: [
            {
              name: 'Easing',
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
              value: 17010,
            },
            {
              name: 'FunctionSequence',
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
              value: 5842,
            },
          ],
        },
        {
          name: 'display',
          children: [
            {
              name: 'DirtySprite',
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
              value: 8833,
            },
            {
              name: 'LineSprite',
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
              value: 1732,
            },
          ],
        },
        {
          name: 'flex',
          children: [
            {
              name: 'FlareVis',
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
              value: 4116,
            },
            {
              name: 'GravityForce',
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
              value: 1336,
            },
          ],
        },
        {
          name: 'physics',
          children: [
            {
              name: 'DragForce',
              value: 1082,
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
            },
            {
              name: 'GravityForce',
              value: 1336,
              children: [
                { name: 'FlareVis', value: 4116 },
                { name: 'GravityForce', value: 1336 },
              ],
            },
          ],
        },
      ],
    };
  }
}
