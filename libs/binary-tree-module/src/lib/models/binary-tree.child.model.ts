/**
 * Child node impl;
 *
 * @export
 * @interface ChildNode
 */
export interface ChildNode {
  name: string;
  value: number | null;
}
