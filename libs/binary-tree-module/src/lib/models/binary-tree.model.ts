import { ChildNode } from './binary-tree.child.model';
import { PlayerNode } from './binary-tree.player.model';

/**
 * ɵBinary Tree impl;
 *
 * @export
 * @interface BinaryTree
 * @extends {ChildNode}
 */
interface BinaryParentTree extends Partial<ChildNode> {
  players: PlayerNode[];
  children: BinaryParentTree[];
}

/**
 *  @type BinaryTree
 */
export type BinaryTree = {
  [P in keyof BinaryParentTree]?: BinaryParentTree[P] | undefined;
};
