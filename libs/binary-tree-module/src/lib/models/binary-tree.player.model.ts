/**
 * Player node impl;
 *
 * @export
 * @interface PlayerNode
 */
export interface PlayerNode {
  id: number;
  uuid: string;
  name?: string | null;
}
