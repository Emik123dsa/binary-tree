
/**
 * @file bt.c
 * 
 * @author Em1k (emil.shari87@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-05-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <emscripten/emscripten.h>
#include "bt.h"

#ifdef _cplusplus
extern "C" {
#endif // _cplusplus

EMSCRIPTEN_KEEPALIVE
_bt_player_t* bt_init(int _bt) {
    _bt_player_t * _pt = (_bt_player_t *)malloc(1 * sizeof( _bt_player_t));
    if (!_pt) exit(1);
    _pt->id = 1;
    _pt->name = "Vova";
    _pt->uuid = "123123123";
    return _pt;
}

EMSCRIPTEN_KEEPALIVE
int sum(int a, int b) {
    return a * b;
}

EMSCRIPTEN_KEEPALIVE 
char * hello() {
    return "123";
}


int main(int argc, char**argv) {
 

    // printf("%s", _pt->id);
    
    // free(_pt);

    return 0;
}
#ifdef _cplusplus
}
#endif // _cplusplus