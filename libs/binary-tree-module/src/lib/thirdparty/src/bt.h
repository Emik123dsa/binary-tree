/**
 * @file bt.h
 * @author Em1k (emil.shari87@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2021-05-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef _BT_H

#define _BT_H

/**
 * @brief binary tree player
 * 
 * @struct _bt_player_t;
 */
typedef struct {
    int id;
    char* uuid;
    char* name;
} _bt_player_t;

/**
 * @brief binary tree node
 * @struct _bt_node_t;
 */
typedef struct {
    int id;
    int value;
    char* name;
    struct _bt_node_t ** children;
    struct _bt_player_t ** players; 
} _bt_node_t;

/**
 * @brief add new node
 * 
 * @param id 
 * @return void* 
 */
void* bt_add_node(_bt_node_t* _node);

/**
 * @brief remove node
 * 
 * @param id 
 * @return void* 
 */
void* bt_remove_node(int * id);
/**
 * @brief add one player to node
 * 
 * @param _player 
 * @return void* 
 */
void* bt_add_player_to_node(int* id, _bt_player_t* _player);

/**
 * @brief add players to node
 * 
 * @param _players 
 * @return void* 
 */
void* bt_add_players_to_node(_bt_player_t** _players);

/**
 * @brief remove players 
 * 
 * @return void* 
 */
void* bt_remove_players_from_node(int * id);

/**
 * @brief remove players 
 * 
 * @return void* 
 */
void* bt_remove_player_from_node(int * id, int * _player_id);

/**
 * @brief 
 * 
 * @param _bt 
 * @return void* 
 */
_bt_player_t* bt_init(int _bt);

void* bt_free(_bt_node_t ** _bnt);

#endif // !_bt