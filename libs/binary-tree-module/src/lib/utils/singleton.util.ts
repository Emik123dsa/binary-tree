/**
 * Singleton service;
 *
 * @export
 * @class AbstractSingleton
 * @template T
 */
export class ɵSingleton<T extends { new (...args: unknown[]): {} } = any> {
  /**
   * Creates an instance;
   *
   * @protected
   * @static
   * @type {AbstractSingleton}
   * @memberof AbstractSingleton
   */
  protected static _instance: ɵSingleton;

  /**
   * Get instance of service;
   *
   * @static
   * @returns {AbstractSingleton}
   * @memberof AbstractSingleton
   */
  public static getInstance(): ɵSingleton {
    if (ɵSingleton._instance) {
      ɵSingleton._instance = new ɵSingleton();
    }
    return ɵSingleton._instance;
  }

  /**
   * Creates an instance of Singleton.
   *
   * @memberof Singleton
   */
  private constructor() {}
}
