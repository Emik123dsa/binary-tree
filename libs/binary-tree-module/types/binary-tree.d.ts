/**
 * Creates a namespace of
 * binary tree;
 */
declare namespace BinaryTree {
  /**
   * Child node impl;
   *
   * @export
   * @interface ɵChildNode
   */
  export interface ChildNode {
    name: string;
    value: number | null;
  }

  /**
   * Player node impl;
   *
   * @export
   * @interface ɵPlayerNode
   */
  export interface PlayerNode {
    id: number;
    uuid: string;
    name?: string | null;
  }

  /**
   * Creates parent node;
   *
   * @interface ParentNode
   * @extends {Partial<ChildNode>}
   */
  interface ParentNode extends Partial<ChildNode> {
    players: ɵPlayerNode[];
    children: ParentNode[];
  }

  /**
   *  @type Root
   */
  export type Root = {
    [P in keyof ParentNode]?: ParentNode[P] | undefined;
  };
}
