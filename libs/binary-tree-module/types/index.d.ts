import './lib';
import './binary-tree';
declare module 'vue/types/vue' {
  interface Vue {
    $zhopa: () => void;
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<> {
    $zhopa: () => void;
  }
}

declare function VueBinaryTree(V: typeof Vue): void;
