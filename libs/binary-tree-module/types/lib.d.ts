declare module 'vue/types/vue' {
  interface Vue {
    $zhopa: () => void;
  }
}

declare module 'vue/types/options' {
  interface ComponentOptions<> {
    $zhopa: () => void;
  }
}

declare module 'vue/types/vnode' {}
